import time

class Arrival_info:
    def __init__(self, headsign, rtState, rtArrival, schArrival, routeNumber, ETA):
        self.headsign = headsign
        self.rtState = rtState
        self.rtArrival = rtArrival
        self.schArrival = schArrival
        self.routeNumber = routeNumber
        if(self.rtState == 'UPDATED'):
            self.ETA = int((rtArrival - (time.localtime().tm_hour*3600+time.localtime().tm_min*60+time.localtime().tm_sec))/60)     
        if(self.rtState == 'SCHEDULED'):
            self.ETA = int((schArrival - (time.localtime().tm_hour*3600+time.localtime().tm_min*60+time.localtime().tm_sec))/60)
        