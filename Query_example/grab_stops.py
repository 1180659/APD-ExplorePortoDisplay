from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport
from operator import itemgetter
import csv

transport = RequestsHTTPTransport(
    url="https://otp.services.porto.digital/otp/routers/default/index/graphql",
    verify=True,
    retries=3,
)

metro_stopString = "1:"
bus_stopString = "2:"
metroStops = []
busStops = []

client = Client(transport=transport, fetch_schema_from_transport=True)

def query_execute(client):
    
    query_string = f"""
                query Query{{
                    stops{{
                      gtfsId
                      name
                      zoneId
                    }}
    }}
    """
    query = gql(query_string)
    #Query to the Explore Porto GraphQL
    result = client.execute(query)
    
    #Extraction of the info of each arrival and append to arrivals_list
    for n in (result['stops']):
        if metro_stopString in n['gtfsId']:
            metroStops.append(n)
        
        elif bus_stopString in n['gtfsId']:
            busStops.append(n)
        pass

    busStops_sorted = sorted(busStops, key=itemgetter('zoneId'))   
    metroStops_sorted = sorted(metroStops, key=itemgetter('gtfsId'))   

    with open('metroStops.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Name', 'GTFSid', 'ZoneId'])
        for n in (metroStops_sorted):
            writer.writerow([n['name'], n['gtfsId'], n['zoneId']])

    with open('busStops.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Name', 'GTFSid', 'ZoneId'])
        for n in (busStops_sorted):
            writer.writerow([n['name'], n['gtfsId'], n['zoneId']])
        
if __name__ == "__main__": 
    query_execute(client)