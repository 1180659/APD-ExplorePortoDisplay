#!/usr/bin/env python
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport
from classes import Arrival_info
import time
import sys

transport = RequestsHTTPTransport(
    url="https://otp.services.porto.digital/otp/routers/default/index/graphql",
    verify=True,
    retries=3,
)

client = Client(transport=transport, fetch_schema_from_transport=True)


def query_execute(stop_id, actual_time, client):
    
    query_string = f"""
                query Query{{
                    stop(id: {stop_id}) {{
                      name
                      code
                      stoptimesWithoutPatterns(startTime: {actual_time} , numberOfDepartures: 7, timeRange: 43200) {{
                        headsign
                        realtimeState
                        realtimeArrival
                        scheduledArrival
                        trip {{
                          routeShortName
                        }}
                     }}
                  }}
    }}
    """
    query = gql(query_string)
    #Query to the Explore Porto GraphQL
    result = client.execute(query)
        
    #List of the next 5 arrivals
    arrivals_list = []
    
    #Extraction of the info of each arrival and append to arrivals_list
    for n in (result['stop']['stoptimesWithoutPatterns']):
        x = Arrival_info(n['headsign'], n['realtimeState'], n['realtimeArrival'], n['scheduledArrival'], n['trip']['routeShortName'], 0)
        arrivals_list.append(x)
        pass
    
    for n in arrivals_list:
            print(f"{n.routeNumber} - {n.headsign} - {n.ETA}")
        
        
if __name__ == "__main__": 
    try:
      stop_id = "\"" + (sys.argv[1]) + "\""
      actual_time = int(time.time())
      query_execute(stop_id, actual_time, client)
    except:
      if len(sys.argv)<2:
        print("Stop Id not defined")
        sys.exit(1)
      else:
        print("Stop Id incorrect")
        sys.exit(1)

    
