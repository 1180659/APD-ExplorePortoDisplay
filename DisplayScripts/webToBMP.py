from html2image import Html2Image
from PIL import Image
import logging
from waveshare_epd import epd7in5_V2
import time
from PIL import Image
import signal
import time
import os
import sys

os.chdir('/home/rr2/APD-ExplorePortoDisplay/DisplayScripts')

MAX_RETRIES = 3  # maximum number of retries for importing image
TIMEOUT = 5  # timeout in seconds


img_name = ("monitor_" + str(int(time.time())) + ".png")
img_path = "imageDump/"

# create Html2Image object with custom flags
hti = Html2Image(
    custom_flags=['--virtual-time-budget=5000', '--hide-scrollbars'], size=(800, 960)
)
hti.output_path = 'imageDump'

# define signal handler for timeout
def timeout_handler(signum, frame):
    raise TimeoutError
            
def get_imageFromURL():
    # try to import the image, retrying if it fails or if the import takes longer than the timeout
    retries = 0
    while True:
        try:
            signal.signal(signal.SIGALRM, timeout_handler)
            signal.alarm(TIMEOUT)
            hti.screenshot(url='https://matkamonitori.digitransit.fi/view?cont=FXvN9-dHy-Z6JpRTZ68rbA==', save_as=img_name)
            signal.alarm(0)  # cancel the timeout
            break
        except TimeoutError:
            retries += 1
            if retries >= MAX_RETRIES:
                # if the maximum number of retries is reached, print an error message and exit
                print("Error: Screenshot failed after", MAX_RETRIES, "attempts")
                exit(1)


def detectBlankImage():
    # try to open the image, retrying if it fails or if the image is blank or if opening the image takes longer than the timeout
    retries = 0
    while True:
        try:
            signal.signal(signal.SIGALRM, timeout_handler)
            signal.alarm(TIMEOUT)
            image = Image.open(img_path + img_name)
            signal.alarm(0)  # cancel the timeout
            gray_image = image.convert('L')

            # get the pixel values as a list of integers
            pixels = list(gray_image.getdata())

            # check if all pixel values are 255 (white)
            if all(p == 255 for p in pixels):  # check if the image is blank
                retries += 1
                if retries >= MAX_RETRIES:
                    # if the maximum number of retries is reached, print an error message and exit
                    print("Error: imported image is blank. retried to screenshot", MAX_RETRIES, "times")
                    exit()
                else:
                    # retry importing the image if it is blank
                    get_imageFromURL()
                    continue
                    
            break
        except TimeoutError:
            retries += 1
            if retries >= MAX_RETRIES:
                # if the maximum number of retries is reached, print
                print("Error: opening image timed out after", MAX_RETRIES, "attempts")
                exit(1)
    return image

def convertToBMP(image):
    #Convert RGB to Black and White
    image1 = image.convert(mode="1", dither= Image.Dither.NONE)
    width, height = image.size
    height = int(height / 2)
    halfTop1 = image1.crop((0, 6, width, height+6))
    halfBottom1 = image1.crop((0, height, width, height*2))

    image1.save(img_path + img_name)

    #Convert to MBP
    halfBottomBMP = halfBottom1.tobitmap()
    halfTopBMP = halfTop1.tobitmap()

    #halfTop1.show()
    #halfBottom1.show()
    
    return halfTopBMP, halfBottomBMP


if __name__ == "__main__": 
    get_imageFromURL()
    imageNotBlank = detectBlankImage()
    imgTop, imgBot = convertToBMP(imageNotBlank)
    exit(0)

# ~ try:
    # ~ logging.info("epd7in5_V2 Display")
    # ~ epdBottom = epd7in5_V2.EPD(7)
    # ~ epdTop = epd7in5_V2.EPD(8)
    
    # ~ logging.info("init and Clear")
    # ~ epdBottom.init()
    # ~ epdTop.init()
    # ~ epdBottom.Clear()
    # ~ epdTop.Clear()

    # ~ epdBottom.display(epdBottom.getbuffer(halfBottomBMP))
    # ~ epdTop.display(epdTop.getbuffer(halfTopBMP))
    # ~ time.sleep(2)

    # ~ logging.info("Goto Sleep...")
    # ~ epdBottom.sleep()
    # ~ epdTop.sleep()
    # ~ time.sleep(3)
        
    # ~ epdBottom.Dev_exit()
    # ~ epdTop.Dev_exit()
    
# ~ except IOError as e:
    # ~ logging.info(e)
    
# ~ except KeyboardInterrupt:    
    # ~ logging.info("ctrl + c:")
    # ~ epd7in5_V2.epdconfig.module_exit()
    # ~ exit()


